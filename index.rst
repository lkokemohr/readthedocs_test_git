Godot Docs – *3.0* branch
=========================

.. tip:: This is the documentation for the stable 3.0 branch.
         Looking for the documentation of the current **development** branch?
         `Have a look here <http://docs.godotengine.org/en/latest>`_.
         For the stable 2.1 branch, `it's here <http://docs.godotengine.org/en/2.1>`_.

Welcome to the official documentation of Godot Engine, the free and open source
community-driven 2D and 3D game engine! If you are new to this documentation,
we recommend that you read the :ref:`introduction page <doc_about_intro>` to
get an overview of what this documentation has to offer.

The table of contents below and in the sidebar should let you easily access the
documentation for your topic of interest. You can also use the search function
in the top left corner.

.. note:: Godot Engine is an open source project developed by a community of
          volunteers. It means that the documentation team can always use your
          feedback and help to improve the tutorials and class reference. If
          you do not manage to understand something, or cannot find what you
          are looking for in the docs, help us make the documentation better
          by letting us know!

          Submit an issue or pull request on the `GitHub repository
          <https://github.com/godotengine/godot-docs/issues>`_,
          help us `translating present documentation
          <https://hosted.weblate.org/engage/godot-engine/>`_ into your
          language, or discuss with us on either the ``#documentation``
          channel on `Discord <https://discord.gg/zH7NUgz>`_, or the
          ``#godotengine-doc`` channel on `irc.freenode.net
          <http://webchat.freenode.net/?channels=#godotengine-doc>`_!